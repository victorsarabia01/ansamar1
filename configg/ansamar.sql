-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         8.0.30 - MySQL Community Server - GPL
-- SO del servidor:              Win64
-- HeidiSQL Versión:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para ansamar
CREATE DATABASE IF NOT EXISTS `ansamar` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ansamar`;

-- Volcando estructura para tabla ansamar.cita
CREATE TABLE IF NOT EXISTS `cita` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_paciente` int DEFAULT '0',
  `id_consultorio` int DEFAULT '0',
  `id_turno` int DEFAULT '0',
  `id_doctor` int DEFAULT '0',
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cita_paciente` (`id_paciente`),
  KEY `FK_cita_consultorio` (`id_consultorio`),
  KEY `FK_cita_turno` (`id_turno`),
  KEY `FK_cita_doctor` (`id_doctor`),
  CONSTRAINT `FK_cita_consultorio` FOREIGN KEY (`id_consultorio`) REFERENCES `consultorio` (`id`),
  CONSTRAINT `FK_cita_doctor` FOREIGN KEY (`id_doctor`) REFERENCES `doctor` (`id`),
  CONSTRAINT `FK_cita_paciente` FOREIGN KEY (`id_paciente`) REFERENCES `paciente` (`id`),
  CONSTRAINT `FK_cita_turno` FOREIGN KEY (`id_turno`) REFERENCES `turno` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- Volcando datos para la tabla ansamar.cita: ~14 rows (aproximadamente)
INSERT INTO `cita` (`id`, `id_paciente`, `id_consultorio`, `id_turno`, `id_doctor`, `fecha`) VALUES
	(1, 1, 2, 2, 1, NULL),
	(2, 1, 1, 1, 1, '2023-07-11'),
	(3, 1, 3, 2, 2, '2023-07-10'),
	(4, 1, 1, 1, 1, '2023-07-10'),
	(5, 1, 1, 1, 1, '2023-07-10'),
	(6, 4, 2, 2, 2, '2023-07-27'),
	(7, 5, 2, 2, 2, '2023-07-11'),
	(8, 6, 2, 1, 2, '2023-07-11'),
	(9, 7, 1, 1, 1, '2023-07-10'),
	(10, 8, 1, 1, 1, '2023-07-11'),
	(11, 2, 1, 1, 1, '2023-07-11'),
	(12, 9, 1, 1, 1, '2023-07-12'),
	(13, 10, 1, 1, 1, '2023-07-11'),
	(14, 11, 1, 1, 1, '2023-07-12'),
	(15, 12, 1, 1, 1, '2023-07-11');

-- Volcando estructura para tabla ansamar.consultorio
CREATE TABLE IF NOT EXISTS `consultorio` (
  `id` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `direccion` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- Volcando datos para la tabla ansamar.consultorio: ~3 rows (aproximadamente)
INSERT INTO `consultorio` (`id`, `descripcion`, `direccion`) VALUES
	(1, 'Garabatal', 'Sur'),
	(2, 'Cerritos Blancos', 'Oeste'),
	(3, '51', 'Centro');

-- Volcando estructura para tabla ansamar.doctor
CREATE TABLE IF NOT EXISTS `doctor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `apellido` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `fechaNac` date DEFAULT NULL,
  `tlfno` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `status` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- Volcando datos para la tabla ansamar.doctor: ~3 rows (aproximadamente)
INSERT INTO `doctor` (`id`, `nombre`, `apellido`, `fechaNac`, `tlfno`, `email`, `status`) VALUES
	(1, 'Katherine', 'Villareal', '2023-07-10', '04245208619', 'katerine@gmail.com', 1),
	(2, 'Pedro', 'Perez', '2023-07-10', '04121507389', 'pedroperez@gmail.com', 1);

-- Volcando estructura para tabla ansamar.paciente
CREATE TABLE IF NOT EXISTS `paciente` (
  `id` int NOT NULL AUTO_INCREMENT,
  `cedula` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `nombres` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `apellidos` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `tlfn` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- Volcando datos para la tabla ansamar.paciente: ~8 rows (aproximadamente)
INSERT INTO `paciente` (`id`, `cedula`, `nombres`, `apellidos`, `tlfn`, `email`) VALUES
	(1, '22186490', 'victor ', 'sarabia', '04245208619', NULL),
	(2, '5454', 'sadasd', 'sdsa', '54546', NULL),
	(3, '5654', 'asdf', 'asd', '54546', NULL),
	(4, '123456', 'ASDASD', 'ASDASD', '5454546', NULL),
	(5, '456789', 'ASDFSD', 'SDFSDF', '456546', NULL),
	(6, '1111', 'ASDF', 'sdf', '555', NULL),
	(7, '5454544', 'ASDFASDF', 'DASFDF', '545454', NULL),
	(8, '11111111', 'asdasd', 'asdfasd', '515454', NULL),
	(9, '454545', 'sdfsf', 'dfsdf', '55454', NULL),
	(10, '54545454', 'adsfadf', 'safasdf', '5154', NULL),
	(11, '55454', 'sdfsdf', 'sdffsd', '5454', NULL),
	(12, '23835353', 'Aura Elena ', 'Caldera Linares', '04121507389', NULL);

-- Volcando estructura para tabla ansamar.turno
CREATE TABLE IF NOT EXISTS `turno` (
  `id` int NOT NULL,
  `descripcion` varchar(50) COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- Volcando datos para la tabla ansamar.turno: ~2 rows (aproximadamente)
INSERT INTO `turno` (`id`, `descripcion`) VALUES
	(1, 'mañana'),
	(2, 'tarde');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
