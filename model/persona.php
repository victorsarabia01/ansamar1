<?php 
	
	//namespace modeld;
	class persona{

		
		public $CNX;
		public $cedula_cliente;
		
		public $id;
		public $cedula;
		public $cedulax;
		public $nombre;
		public $apellido;
		public $direccion;
		public $correo;
		public $telefono;
		public $status;
		public $descripcion;
		public $monto;
		public $monto1;
		public $montoBs;
		public $id_cliente;
		public $precioDolarA;
		public $montoEnDolares;
		public $balanceNegativo;
		public $balancePositivo;
		public $balance;
		public $selectCliente1;
		public $selectCliente;

		public $nombresApellidos;
		public $tlfn;
		

		public function __construct(){
			try {
				$this->CNX = conexion::conectar();
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		// FUNCION PARA VERIFICAR QUE NO EXISTA LA CEDULA EN BD EVITAR DUPLICADOS
		public function cargarCedula($cedula){
			try {
				$query="SELECT cedula FROM cliente WHERE cedula=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($cedula));
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//FUNCION PARA REGISTRAR CLIENTE EN BD

		public function registrar(persona $data){
			try {
				$query="INSERT into cliente (cedula,nombresApellidos,direccion,correo,tlfn,status) values (?,?,?,?,?,?)";
				$this->CNX->prepare($query)->execute(array($data->cedula,$data->nombre,$data->direccion,$data->correo,$data->telefono,$data->status));
				//$this->CNX->prepare($query)->execute(array($data->cedula,$data->nombre.' '.$data->apellido,$data->direccion,$data->correo,$data->telefono,$data->status));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//FUNCIONMENSAJE DE CEDULA DE CLIENTE YA REG EN BD REDIRECCIONA A REGISTRAR CLIENTE

		public function mensajeAmigoRegistrado(){
				echo "<script>
				
                alert('Esta Cedula Ya esta REGISTRADA');
                 setTimeout( function() { window.location.href = 'index.php?c=nuevo'; }, 1000 );
    			</script>";

    			echo "<script>
                console.log('Cliente Ya Registrado');
    			</script>";
		}

		//FUNCION LISTAR CLIENTES EN BD
		public function listarClientes(){
			try {
				$query="SELECT * FROM cliente order by id desc";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		// CONSULTAR REGISTRO DE CLIENTES FETCH
		public function cargarIdCliente($id){
			try {
				$query="SELECT * FROM cliente WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//CONSULTAR REGISTRO DE CLIENTE SEGUN ID FECHT ALL
		public function cargarIdCliente1($id){
			try {
				$query="SELECT * FROM cliente WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//ACTUALIZAR REGISTRO DEL CLIENTE
		public function actualizarCliente(persona $data){
			try {
				$query="UPDATE cliente set cedula=?, nombresApellidos=?, direccion=?, correo=?, tlfn=?, status=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array($data->cedulax,$data->nombre,$data->direccion,$data->correo,$data->telefono,$data->status,$data->id));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//VERIFICAR SI EXISTE LA CEDULA REGISTRADA EN BD
		public function cargarCedula2($cedula){
			try {
				$query="SELECT cedula FROM cliente WHERE cedula=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($cedula));
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//FUNCIONMENSAJE DE CEDULA DE CLIENTE YA REG EN BD REDIRECCIONA A LISTAR CLIENTES
		public function mensajeAmigoRegistrado1(){
				echo "<script>
				
                alert('Esta Cedula Ya esta REGISTRADA');
                 setTimeout( function() { window.location.href = 'index.php?c=listarClientes'; }, 1000 );
    			</script>";

    			echo "<script>
                console.log('Cliente Ya Registrado');
    			</script>";
		}

		//CONSULTAR BALANCE DE CREDITO DEL CLIENTE
		public function consultarBalance($id){
			try {
				$query="SELECT balance FROM cliente WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetchAll(PDO::FETCH_OBJ);
				//return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//ELIMINAR CLIENTE DE LA BD
		public function deleteCliente($id){
			try {

				$query="DELETE FROM cliente WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetchAll(PDO::FETCH_OBJ);

			} catch (Exception $e) {
				die($e->getMessage());
			}
		}


		//MENSAJE DE CLIENTE ELIMINADO DE LA BD NO POSEE DEUDA O MONTO POSITIVO
		public function clienteEliminado(){
				echo "<script>
				
                alert('Cliente Eliminado');
                 setTimeout( function() { window.location.href = 'index.php?c=listarClientes'; }, 1000 );
    			</script>";

    			echo "<script>
                console.log('Cliente Ya Registrado');
    			</script>";
		}

		//CLIENTE CON MONTO EN POSITIVO NO SE PUEDE ELIMINAR
		public function clienteEnPositivo(){
				echo "<script>
				
                alert('Cliente en Positivo');
                 setTimeout( function() { window.location.href = 'index.php?c=listarClientes'; }, 1000 );
    			</script>";

    			echo "<script>
                console.log('Cliente Ya Registrado');
    			</script>";
		}

		//CLIENTE CON MONTO EN NEGATIVO NO SE PUEDE ELIMINAR
		public function mensajeNoPuedeEliminar(){
				echo "<script>
				
                alert('No Puede Eliminar Cliente con Deuda');
                 setTimeout( function() { window.location.href = 'index.php?c=listarClientes'; }, 1000 );
    			</script>";

    			echo "<script>
                console.log('Cliente Ya Registrado');
    			</script>";
		}
		
		//CARGAR REGISTRO DE DEUDA
		public function cargarIdDeuda($id){
			try {
				$query="SELECT * FROM deuda WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		
		
		//CARGAR REGISTRO DE ABONO
		public function cargarIdPago($id){
			try {
				$query="SELECT * FROM abonado WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//RESGISTRAR DEUDA DE CLIENTE EN BD
		public function registrarDeuda(persona $data){
			try {
				$query="INSERT into deuda (fecha,descripcion,monto,montoBs,id_cliente) values (?,?,?,?,?)";
				$this->CNX->prepare($query)->execute(array($data->fecha,$data->descripcion,$data->montoEnDolares,$data->monto,$data->selectCliente));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//ACTUALIZAR DATOS DE DEUDA DE CLIENTE
		public function actualizarDatosDeuda(persona $data){
			try {
				$query="UPDATE deuda set fecha=?,descripcion=?,monto=?,montoBs=?,id_cliente=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array($data->fecha,$data->descripcion,$data->montoEnDolares,$data->monto,$data->selectCliente1,$data->idDeuda));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//SUMAR TODOS LOS MONTOS ABONADOS DEL CLIENTE
		public function balancePositivoR($id){
			try {
				$query="SELECT SUM(a.montoAbonado) as montox FROM abonado AS a WHERE a.id_cliente=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//SUMAR TODAS LAS DEUDAS DEL CLIENTE
		public function balanceNegativoR($id){
			try {
				$query="SELECT SUM(d.monto) as deuda FROM deuda AS d WHERE d.id_cliente=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetchAll(PDO::FETCH_OBJ);
				//return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//ACTUALIZAR BALANCE DEL CLIENTE 
		public function actualizarBalance(persona $data){
			try {
				$query="UPDATE cliente set balance=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array($data->balance,$data->selectCliente));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//ACTUALIZAR BALANCE DEL CLIENTE DIFIERE DEL ANTERIOR POR EL SELECTCLIENTE1 QUE ESTA DISABLED EN LA VISTA
		public function actualizarBalance1(persona $data){
			try {
				$query="UPDATE cliente set balance=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array($data->balance,$data->selectCliente1));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//ELIMINAR UNA DEUDA DEL CLIENTE
		public function deleteDeuda($id){
			try {

				$query="DELETE FROM deuda WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetchAll(PDO::FETCH_OBJ);

			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//ACTUALIZAR DATOS DE ABONO
		public function actualizarDatosAbono(persona $data){
			try {
				$query="UPDATE abonado set fecha=?,descripcion=?,montoAbonado=?,montoBs=?,id_cliente=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array($data->fecha,$data->descripcion,$data->montoEnDolares,$data->monto,$data->selectCliente1,$data->idAbono));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//REGISTRAR ABONO DEL CLIENTE
		public function registrarAbono(persona $data){
			try {
				$query="INSERT into abonado (fecha,descripcion,montoAbonado,montoBs,id_cliente) values (?,?,?,?,?)";
				$this->CNX->prepare($query)->execute(array($data->fecha,$data->descripcion,$data->montoEnDolares,$data->monto,$data->selectCliente));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//ELIMINAR ABONO DEL CLIENTE
		public function deleteAbono($id){
			try {

				$query="DELETE FROM abonado WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetchAll(PDO::FETCH_OBJ);

			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		
		//CARGAR REGISTRO EN LISTA PLEGABLE EN LAS VISTAS
		public function cargarCliente(){
			try {
				$query="SELECT c.id as idCliente, c.nombresApellidos as nombre from cliente as c";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

				
		//LISTAR REGISTROS DE LA BD
		public function listarClientesDeudores(){
			try {
				$query="SELECT * FROM cliente WHERE balance>0 order by balance desc";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function listarClientesDeudoresMas100(){
			try {
				$query="SELECT * FROM cliente WHERE balance>0 and balance>100 order by balance desc";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function listarClientesDeudoresMas15Dias(){
			try {
				$query="SELECT c.id,c.cedula,c.nombresApellidos,c.direccion,c.tlfn,c.balance, MAX(a.fecha) AS fecha FROM abonado AS a INNER JOIN cliente AS c WHERE a.id_cliente=c.id and c.balance >0 GROUP BY c.id";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function listarUltimosPagos(){
			try {
				$query="SELECT a.id,a.descripcion, a.fecha,a.montoAbonado,a.montoBs,a.id_cliente as idCliente,c.cedula,c.nombresApellidos FROM abonado AS a INNER JOIN cliente AS c WHERE a.id_cliente=c.id ORDER BY fecha DESC limit 20";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function listarUltimasDeudas(){
			try {
				$query="SELECT a.id, a.fecha,a.descripcion,a.monto,a.montoBs,c.id as idCliente,c.cedula,c.nombresApellidos FROM deuda AS a INNER JOIN cliente AS c WHERE a.id_cliente=c.id ORDER BY fecha DESC limit 20";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function consultarPrecioDolarActual(){
			try {
				$query="SELECT precioDolar FROM preciodolar WHERE id = (SELECT MAX(id) FROM preciodolar)";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		public function consultarMontoTotalPorCobrar1(){
			try {
				$query="SELECT SUM(balance) as montoTotal FROM cliente WHERE balance > 0";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function consultarMontoTotalPorCobrar(){
			try {
				$query="SELECT SUM(balance) as montoTotal FROM cliente WHERE balance > 0";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function consultarCantidadClientesDeudores(){
			try {
				$query="SELECT COUNT(id) as cant FROM cliente WHERE balance>0 ";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		//ACTUALIZAR EL PRECIO DEL DOLAR ACTUAL EN BS
		public function actualizarPrecioDolar(persona $data){
			try {
				$query="INSERT into preciodolar (fecha,precioDolar) values (?,?)";
				$this->CNX->prepare($query)->execute(array(date('d/m/y'),$data->precioDolarA));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}











		
			public function registrarAbono1(persona $data){
			try {
				$query="INSERT into abonado (fecha,descripcion,montoAbonado,montoBs,id_cliente) values (?,?,?,?,?)";
				$this->CNX->prepare($query)->execute(array($data->fecha,$data->descripcion,$data->montoEnDolares,$data->monto,$data->selectCliente1));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		

		public function balancePositivoRR(){
			try {
				$query="SELECT SUM(a.montoAbonado) as montox FROM abonado AS a WHERE a.id_cliente=1";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

			public function cargarCedula1($cedula){
			try {
				$query="SELECT cedula FROM cliente WHERE cedula=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($cedula));
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		
		

		










































































































































































		




		/*public function guardarPrecioDolarActual(persona $data){
			try {
				$query="INSERT into deuda (fecha,descripcion,monto,id_cliente) values (?,?,?,?)";
				$this->CNX->prepare($query)->execute(array($data->fecha,$data->descripcion,$data->montoEnDolares,$data->selectCliente));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}*/

		/*public function cargarCedula($cedula){
			try {
				$query="SELECT cedula FROM amigos WHERE cedula=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($cedula));
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}*/


		













		/*public function listar(){
			try {
				$query="SELECT * FROM amigos";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}*/
		public function listar(){
			try {
				//$query="SELECT a.id,a.cedula,a.nombre,a.apellido,a.sexo,a.direccion,a.telefono,a.fecha_nacimiento FROM amigos as a INNER JOIN miembros as m WHERE a.id!=m.amigo_id";
				$query="SELECT a.id,a.cedula,a.nombre,a.apellido,a.sexo,a.direccion,a.codTlfn,a.telefono,a.fecha_nacimiento,a.fecha_ingreso,a.llegoPor FROM amigos as a WHERE a.estatus=0 order by a.fecha_ingreso desc";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function listarTodos(){
			try {
				//$query="SELECT a.id,a.cedula,a.nombre,a.apellido,a.sexo,a.direccion,a.telefono,a.fecha_nacimiento FROM amigos as a INNER JOIN miembros as m WHERE a.id!=m.amigo_id";
				$query="SELECT a.id,a.cedula,a.nombre,a.apellido,a.sexo,a.direccion,a.codTlfn,a.telefono,a.fecha_nacimiento,a.fecha_ingreso,a.llegoPor FROM amigos as a order by a.fecha_ingreso desc";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function listarActividades(){
			try {
				$query="SELECT a.id,a.nombre,a.direccion,a.fecha,a.hora,a.descripcion,am.nombre as nombreEncargado FROM actividades as a INNER JOIN amigos as am where a.id_encargado=am.id and a.status=1";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function listarActividadesInactivas(){
			try {
				$query="SELECT a.id,a.nombre,a.direccion,a.fecha,a.hora,a.descripcion,am.nombre as nombreEncargado FROM actividades as a INNER JOIN amigos as am where a.id_encargado=am.id and a.status=0";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function listarDonaciones(){
			try {
				$query="SELECT d.id,d.detalles,d.cantidad,d.fecha,a.nombre as nombreDonante from donaciones as d INNER JOIN amigos as a where d.donante_id=a.id and d.status=1";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function listarDonacionesInactivas(){
			try {
				$query="SELECT d.id,d.detalles,d.cantidad,d.fecha,a.nombre as nombreDonante from donaciones as d INNER JOIN amigos as a where d.donante_id=a.id and d.status=0";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function listarDonacionesAsignadas(){
			try {
				$query="SELECT od.id,d.detalles as nombreDonacion,od.descripcion,od.cantidad,od.fechaEntregada,a.nombre from donaciones as d INNER JOIN observacion_donacion as od INNER JOIN actividades as a where d.id=od.donacion_id and a.id=od.actividad_id order by od.donacion_id desc";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function listarGrupos(){
			try {
				$query="SELECT gf.id,gf.nombre as nombre,gf.direccion as direccion,a.nombre as nombreLider,z.nombre as nombreZona FROM grupos_familiares as gf INNER JOIN amigos as a INNER JOIN zonas as z WHERE gf.lider_id=a.id AND gf.id_zona=z.id";
				/*$query="SELECT gf.id,gf.nombre as nombre,gf.direccion as direccion,a.nombre as nombreLider,z.nombre as nombreZona, COUNT(gfa.amigo_id) FROM grupos_familiares as gf INNER JOIN amigos as a INNER JOIN zonas as z INNER JOIN grupos_familiares_amigos as gfa WHERE gf.lider_id=a.id AND gf.id_zona=z.id AND gfa.grupos_familiares_id=gf.id and gfa.amigo_id=a.id";*/
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function listarGrupos1($id){
			try {
				$query="SELECT count(amigo_id) as cant from grupos_familiares_amigos where grupos_familiares_id =?";
				/*$query="SELECT gf.id,gf.nombre as nombre,gf.direccion as direccion,a.nombre as nombreLider,z.nombre as nombreZona, COUNT(gfa.amigo_id) FROM grupos_familiares as gf INNER JOIN amigos as a INNER JOIN zonas as z INNER JOIN grupos_familiares_amigos as gfa WHERE gf.lider_id=a.id AND gf.id_zona=z.id AND gfa.grupos_familiares_id=gf.id and gfa.amigo_id=a.id";*/
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function delete ($id){
			try {
				$query="DELETE FROM grupos_familiares_amigos WHERE amigo_id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));

				$query="DELETE FROM amigos WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
			
		}
		public function deleteMiembro ($id){
			try {

				$query="DELETE FROM miembros WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetchAll(PDO::FETCH_OBJ);

			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function cambiarStatus($amigo_id){
			try {
				$query="UPDATE amigos set estatus=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array("0",$amigo_id));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function deleteGrupo ($id){
			try {
				$query="DELETE FROM grupos_familiares WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function deleteDonacionAsignada ($id){
			try {
				$query="DELETE FROM observacion_donacion WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function deleteAmigoDeGrupo ($id){
			try {
				$query="DELETE FROM grupos_familiares_amigos WHERE amigo_id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function deleteActividad ($id){
			try {
				$query="DELETE FROM actividades WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function deleteDonacion ($id){
			try {
				$query="DELETE FROM donaciones WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function cargarIdMiembro($id){
			try {
				/*$query="SELECT * FROM miembros as m INNER JOIN amigos as a WHERE m.id=? and m.amigo_id=a.id";*/
				$query="SELECT m.id as id_miembro,m.fecha_paso_de_fe,m.fecha_bautismo,m.vehiculo,m.nivelAcademico,m.profesion,m.membresia_id,m.cargo_id,m.amigo_id,a.id,a.cedula,a.nombre,a.apellido,a.sexo,a.direccion,a.codTlfn,a.telefono,a.fecha_nacimiento,a.estatus,a.fecha_ingreso,a.llegoPor FROM miembros as m INNER JOIN amigos as a WHERE m.id=? and m.amigo_id=a.id";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function cantMiembros(){
			try {
				$query="SELECT count(id) as cantMiembros FROM amigos where estatus=1";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function cantAmigos(){
			try {
				$query="SELECT count(id) as cantAmigos FROM amigos where estatus=0";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		
		public function cargarID($id){
			try {
				$query="SELECT * FROM amigos WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		
		public function cargarIdActividad($id){
			try {
				$query="SELECT * FROM actividades WHERE id=? and status=1";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function cargarIdActividadInactiva($id){
			try {
				$query="SELECT * FROM actividades WHERE id=? and status=0";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function cargarIdDonacion($id){
			try {
				$query="SELECT * FROM donaciones WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function cargarIdDetallesDonacion($id){
			try {
				$query="SELECT * FROM observacion_donacion WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		/*public function registrar(persona $data){
			try {
				$query="INSERT into amigos (cedula,nombre,apellido,sexo,direccion,telefono,fecha_nacimiento) values (?,?,?,?,?,?,?)";
				$this->CNX->prepare($query)->execute(array($data->cedula,$data->nombre,$data->apellido,$data->sexo,$data->direccion,$data->telefono,$data->fecha_nacimiento));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}*/
		
		
		public function registrarMiembro(persona $data){
			try {
				$query="INSERT into miembros (fecha_paso_de_fe,fecha_bautismo,vehiculo,nivelAcademico,profesion,membresia_id,cargo_id,amigo_id) values (?,?,?,?,?,?,?,?)";
				$this->CNX->prepare($query)->execute(array($data->fecha_paso_de_fe,$data->fecha_bautismo,$data->vehiculo,$data->nivelAcademico,$data->profesion,$data->membresia,$data->id_cargo,$data->id));

				/*$query="UPDATE amigos set estatus=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array("1",$data->id));*/
				$query="UPDATE amigos set cedula=?,nombre=?,apellido=?,sexo=?,direccion=?,codTlfn=?,telefono=?,fecha_nacimiento=?,estatus=?,fecha_ingreso=?,llegoPor=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array($data->cedula,$data->nombre,$data->apellido,$data->sexo,$data->direccion,$data->codTlfn,$data->telefono,$data->fecha_nacimiento,"1",$data->fecha_ingreso,$data->llegoPor,$data->id));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function actualizarDatosAmigo($data){
			try {
				$query="UPDATE amigos set cedula=?,nombre=?,apellido=?,sexo=?,direccion=?,codTlfn=?,telefono=?,fecha_nacimiento=?,fecha_ingreso=?,llegoPor=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array($data->cedula,$data->nombre,$data->apellido,$data->sexo,$data->direccion,$data->codTlfn,$data->telefono,$data->fecha_nacimiento,$data->fecha_ingreso,$data->llegoPor,$data->id));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function actualizarDatosDonacionAsignada($data){
			try {
				$query="UPDATE observacion_donacion set descripcion=?,cantidad=?,fechaEntregada=?,actividad_id=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array($data->descripcion,$data->cantidad,$data->fecha,$data->actividad_id,$data->id));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function asignarDonacionActividad(persona $data){
			try {
				$query="INSERT into observacion_donacion (descripcion,cantidad,fechaEntregada,donacion_id,actividad_id) values (?,?,?,?,?)";
				$this->CNX->prepare($query)->execute(array($data->descripcion,$data->cantidad,$data->fecha,$data->id,$data->actividad_id));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function registrarActividad(persona $data){
			try {
				$query="INSERT into actividades (nombre,direccion,fecha,hora,descripcion,status,id_encargado) values (?,?,?,?,?,?,?)";
				$this->CNX->prepare($query)->execute(array($data->nombre,$data->direccion,$data->fecha,$data->hora,$data->descripcion,"1",$data->lider_id));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function registrarGrupo(persona $data){
			try {
				$query="INSERT into grupos_familiares (nombre,direccion,lider_id,id_zona) values (?,?,?,?)";
				$this->CNX->prepare($query)->execute(array($data->nombre,$data->direccion,$data->id_miembro,$data->id_zona));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function registrarDonacion(persona $data){
			try {
				$query="INSERT into donaciones (detalles,cantidad,donante_id,fecha,status) values (?,?,?,?,?)";
				$this->CNX->prepare($query)->execute(array($data->detalles,$data->cantidad,$data->donante_id,$data->fecha,"1"));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function registrarAmigoEnGrupo(persona $data){
			try {
				$query="INSERT into grupos_familiares_amigos (amigo_id,grupos_familiares_id) values (?,?)";
				$this->CNX->prepare($query)->execute(array($data->id_amigo,$data->id_grupo));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function actualizarDatos($data){
			try {
				$query="UPDATE amigos set cedula=?,nombre=?,apellido=?,sexo=?,direccion=?,telefono=?,fecha_nacimiento=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array($data->cedula,$data->nombre,$data->apellido,$data->sexo,$data->direccion,$data->telefono,$data->fecha_nacimiento,$data->id));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function actualizarDatosGrupo($data){
			try {
				$query="UPDATE grupos_familiares set nombre=?,direccion=?,lider_id=?,id_zona=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array($data->nombre,$data->direccion,$data->id_miembro,$data->id_zona,$data->id_grupo));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function actualizarDatosActividad($data){
			try {
				$query="UPDATE actividades set nombre=?,direccion=?,fecha=?,hora=?,descripcion=?,status=?,id_encargado=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array($data->nombre,$data->direccion,$data->fecha,$data->hora,$data->descripcion,"1",$data->lider_id,$data->id));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function actualizarDatosDonacion($data){
			try {
				$query="UPDATE donaciones set detalles=?,cantidad=?,donante_id=?,fecha=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array($data->detalles,$data->cantidad,$data->donante_id,$data->fecha,$data->id));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		
		public function cargarAmigos(){
			try {
				//$query="SELECT * from amigos where estatus=0";
				$query="SELECT * FROM amigos AS a WHERE a.id NOT IN(SELECT f.amigo_id FROM grupos_familiares_amigos AS f) AND a.estatus=0";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		public function cargarAmigosTablaGrupos(){
			try {
				$query="SELECT * from grupos_familiares_amigos";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function cargarCantGrupos(){
			try {
				$query="SELECT count(id) as cantGrupos from grupos_familiares";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function cargarCantActividades(){
			try {
				$query="SELECT count(id) as cantActividades from actividades where status=1";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function cargarCantActividadesInactivas(){
			try {
				$query="SELECT count(id) as cantActividades from actividades where status=0";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		/*public function cargarAmigosGrupo1($id){
			try {
				//$query="SELECT * FROM amigos as a INNER join grupos_familiares_amigos as gfa where a.id=gfa.amigo_id and gfa.grupos_familiares_id=?";
				$query="SELECT * FROM amigos";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				//$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);

			} catch (Exception $e) {
				die($e->getMessage());
			}
		}*/
		public function cargarAmigosGrupo($id){
			try {
				$query="SELECT g.id as idGrupo,gfa.grupos_familiares_id as id,a.cedula as cedulaAmigo,a.nombre as nombreAmigo,a.apellido as apellidoAmigo, a.id as id_amigo, g.nombre as nombreGrupo FROM amigos as a INNER join grupos_familiares_amigos as gfa INNER JOIN grupos_familiares as g where g.id=gfa.grupos_familiares_id and a.id=gfa.amigo_id and gfa.grupos_familiares_id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
				echo "<script> console.log('paso por aqui'); </script>";
			}
		}
		public function cargarAmigosGrupo1($id){
			try {
				$query="SELECT g.id as idGrupo,gfa.grupos_familiares_id as id,a.cedula as cedula,a.nombre as nombreAmigo,a.apellido as apellido, a.id as id_amigo, g.nombre as nombreGrupo FROM amigos as a INNER join grupos_familiares_amigos as gfa INNER JOIN grupos_familiares as g where g.id=gfa.grupos_familiares_id and a.id=gfa.amigo_id and gfa.grupos_familiares_id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		/*public function cargarCedula($cedula){
			try {
				$query="SELECT cedula FROM amigos WHERE cedula=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($cedula));
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}*/
		public function cargarAmigosGrupoPrueba(){
			try {
				//$query="SELECT a.cedula,a.nombre as nombreAmigo,a.apellido,a.id as id_amigo FROM amigos as a INNER join grupos_familiares_amigos as gfa INNER JOIN grupos_familiares as g where g.id=gfa.grupos_familiares_id and a.id=gfa.amigo_id and gfa.grupos_familiares_id='6'";
				//$query="SELECT a.cedula,a.nombre as nombreAmigo,a.apellido,a.id as id_amigo FROM amigos as a INNER join grupos_familiares_amigos as gfa INNER JOIN grupos_familiares as g where g.id=gfa.grupos_familiares_id and a.id=gfa.amigo_id and gfa.grupos_familiares_id='6'";
				$query="SELECT a.cedula,a.nombre as nombreAmigo,a.apellido,a.id as id_amigo FROM amigos as a";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		public function cargarZonas(){
			try {
				$query="SELECT * FROM zonas";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function cargarAmigosPrueba(){
			try {
				//$query="SELECT * FROM amigos as a inner join cargos where a.id=24";
				//$query="SELECT a.cedula,a.nombre as nombreAmigo,a.apellido,a.id as id_amigo FROM amigos as a";
				$query="SELECT a.cedula,a.nombre as nombreAmigo,a.apellido,a.id as id_amigo FROM amigos as a INNER join grupos_familiares_amigos as gfa INNER JOIN grupos_familiares as g where g.id=gfa.grupos_familiares_id and a.id=gfa.amigo_id and gfa.grupos_familiares_id='4'";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function listarMiembros(){
			try {
				$query="SELECT m.id,a.cedula,a.nombre as nomb,a.apellido,a.sexo,a.direccion,a.telefono,a.fecha_nacimiento,m.fecha_paso_de_fe,m.fecha_bautismo,m.membresia_id,c.nombre,m.amigo_id as idAmigo,me.nombre as nombreMembresia FROM amigos as a INNER JOIN miembros as m INNER JOIN cargos as c INNER JOIN membresias as me WHERE a.id=m.amigo_id and c.id=m.cargo_id and m.membresia_id=me.id order by a.cedula desc";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function cargarCargos(){
			try {
				$query="SELECT * FROM cargos";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function cargarMembresias(){
			try {
				$query="SELECT * FROM membresias";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		public function cargarVehiculos(){
			try {
				$query="SELECT * FROM amigos";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

		/*public function mensajeAmigoRegistrado(){
				echo "<script>
				
                alert('Esta Cedula Ya esta REGISTRADA');
                 setTimeout( function() { window.location.href = 'index.php?c=nuevo'; }, 1000 );
    			</script>";

    			echo "<script>
                console.log('Amigo Ya Registrado');
    			</script>";
		}*/
		/*public function actualizarDatosAmigo($data){
			try {
				$query="UPDATE amigos set cedula=?,nombre=?,apellido=?,sexo=?,direccion=?,telefono=?,fecha_nacimiento=?,fecha_ingreso=?,llegoPor=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array($data->cedula,$data->nombre,$data->apellido,$data->sexo,$data->direccion,$data->telefono,$data->fecha_nacimiento,$data->fecha_ingreso,$data->llegoPor,$data->id));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}*/
		public function actualizarDatosMiembro($data){
			try {
				$query="UPDATE miembros set fecha_paso_de_fe=?,fecha_bautismo=?,vehiculo=?,nivelAcademico=?,profesion=?,membresia_id=?,cargo_id=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array($data->fecha_paso_de_fe,$data->fecha_bautismo,$data->vehiculo,$data->nivelAcademico,$data->profesion,$data->membresia,$data->id_cargo,$data->id_miembro));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function inhabilitarDonacion($id){
			try {
				$query="UPDATE donaciones set status=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array('0',$id));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function habilitarDonacion($id){
			try {
				$query="UPDATE donaciones set status=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array('1',$id));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function inhabilitarActividad($id){
			try {
				$query="UPDATE actividades set status=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array('0',$id));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function habilitarActividad($id){
			try {
				$query="UPDATE actividades set status=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array('1',$id));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

	} 

?>