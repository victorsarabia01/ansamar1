<?php 
//namespace controller;
include_once 'model/model.php';
	//CONTROLADOR MAS FUNCION QUE INVOCA LA VISTAA
	
	class control{
		public $mode;

		public function __construct(){
			$this->mode = new model();
		}

		//INVOCAR A LA VISTA INICIO
		public function index(){
			include_once 'view/homeIndex.php'; 
		}

		//CARGAR LA VISTA PARA AGENDAR UNA CITA

		public function nuevaCita(){
			$alm = new model();
			/*if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarID($_REQUEST['id']);
			}*/
			
			include_once 'view/agendarCita.php';
		}

		//PLANIFICACION

		public function planificacion(){
			$alm = new model();
			/*if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarID($_REQUEST['id']);
			}*/
			
			include_once 'view/planificacion.php';
		}

		//LISTAR PLANIFICACION

		public function listarPlanificacion(){
			$alm = new model();
			/*if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarID($_REQUEST['id']);
			}*/
			
			include_once 'view/listarPlanificacion.php';
		}

		//CARGAR LA VISTA PARA MOSTRAR CITAS GARABATAL

		public function mostrarCitasGarabatal(){
			$alm = new model();
			/*if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarID($_REQUEST['id']);
			}*/
			//$alm = $this->mode->listarCitasGarabatal();
			include_once 'view/listarCitasGarabatal.php';
		}


		//GUARDAR REGISTRO DEL CLIENTE
		public function guardar(){
			$alm = new model();
			$alm->cedula_cliente = $this->mode->cargarCedula($_POST['cedula']);
		
			//$alm->id = $_POST['txtID'];
			$alm->cedula = $_POST['cedula'];
			$alm->nombre = $_POST['nombres'];
			$alm->apellido = $_POST['apellidos'];
			//$alm->direccion = $_POST['direccion'];
			//$alm->correo = $_POST['correo'];
			$alm->telefono = $_POST['telefono'];
			$alm->id_consultorio = $_POST['consultorio'];
			$alm->id_turno = $_POST['turno'];
			$alm->id_doctor = $_POST['doctor'];
			$alm->fechaCita = $_POST['fecha'];
			//$alm->status = $_POST['status'];

			$ip = $_SERVER['REMOTE_ADDR'];
			$captcha = $_POST['g-recaptcha-response'];
			$secretkey = "6Lcc4xInAAAAACAgtexHL5FCVBVoR0vPK4wwS8KF";

			$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secretkey&response=$captcha&remoteip=$ip");
			$atributos = json_decode($response,TRUE);

			if(!$atributos['success']){

				/*echo '<div class="alert alert-danger alert-dismissible fade in" role="alert">'; 
				echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
				echo '<span aria-hidden="true">×</span>';
				echo '</button>';
				echo '<strong> Error!</strong> Servidor de datos no econtrado, vuelva a intentar mas tarde. </div>';*/

				echo "<script>  
                            alert('Debe completar el captcha');
                            history.back();
                            </script>";
			
			

			}else if ($alm->id_consultorio==0 || $alm->id_turno==0 || $alm->id_doctor==0 ){
					echo   "<script>  
                            alert('Debe Llenar todo el Formulario');
                            history.back();
                            </script>";

			}else if($alm->cedula_cliente==""){

				//REGISTRAR CLIENTE
				$this->mode->registrar($alm);

				
				foreach ($this->mode->cargarIdPaciente($_POST['cedula']) as $k) : 
					$alm->id_paciente = $k->id;
				endforeach; 

				foreach ($this->mode->contadorCitas($_POST['fecha'],$_POST['turno'],$_POST['consultorio']) as $k) : 
					$alm->contador = $k->contador;
				endforeach;
				if($alm->contador<=2){
					$this->mode->registrarCita($alm);
				echo "<script>
                alert('Cita Registrada');
                 setTimeout( function() { window.location.href = 'index.php'; }, 5000 );
    			</script>";
				}else {

					echo "<script>
								alert('SELECCIONE OTRO TURNO O DIA DIFERENTE');
                
                  history.back();
    			</script>";

				}
			}
			else{
					foreach ($this->mode->cargarIdPaciente($_POST['cedula']) as $k) : 
					$alm->id_paciente = $k->id;
					endforeach; 
					foreach ($this->mode->contadorCitas($_POST['fecha'],$_POST['turno'],$_POST['consultorio']) as $k) : 
					$alm->contador = $k->contador;
				endforeach;
				if($alm->contador<=2){
					$this->mode->registrarCita($alm);
				echo "<script>
                alert('Cita Registrada');
                 setTimeout( function() { window.location.href = 'index.php'; }, 1000 );
    			</script>";
				}else {

					echo "<script>
								alert('SELECCIONE OTRO TURNO O DIA DIFERENTE');
                
                  history.back();
    			</script>";

				}
			}
		}
		

		

		// GUARDAR PLANIFICACION
		public function guardarPlanificacion(){
			$alm = new model();
			
			
			$alm->id_consultorio = $_POST['consultorio'];
			$alm->id_turno = $_POST['turno'];
			$alm->id_doctor = $_POST['doctor'];

			foreach ($this->mode->cargarPlanificacion($_POST['consultorio'],$_POST['turno'],$_POST['doctor']) as $k) : 
					$alm->consulta = $k->consulta;
			endforeach; 

			if ($alm->id_consultorio==0 || $alm->id_turno==0 || $alm->id_doctor==0 ){
					echo   "<script>  
                            alert('Debe seleccionar todas las opciones');
                            history.back();
                            </script>";

			}else{


			if(!$alm->consulta==""){
				echo "<script>
                alert('Ya existe una planificacion con los datos Seleccionados');
                 history.back();
    			</script>";

			}else{
				$this->mode->registrarPlanificacion($alm);
				echo "<script>
                alert('Planificacion Registrada');
                 setTimeout( function() { window.location.href = 'index.php?c=planificacion'; }, 1000 );
    			</script>";

			}
			}

		
		}

		//ELIMINAR REGISTRO DE PLANIFICACION

		public function eliminarPlanificacion(){
			$alm = new model();
			
		$this->mode->deletePlanificacion($_REQUEST['id']);

		include_once 'view/listarPlanificacion.php';

		}









































		// LISTAR REGISTRO DE CLIENTES EN BD
		public function listarClientes(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				//$alm = $this->mode->cargarIdClientes($_REQUEST['id']);
			}
			include_once 'view/clientes.php';
		}


		//CARGAR LA VISTA PARA GUARDAR UN CLIENTE

		public function nuevo(){
			$alm = new persona();
			/*if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarID($_REQUEST['id']);
			}*/
			include_once 'view/save.php';
		}

		

		

		// EDITAR REGISTRO DEL CLIENTE EN BD
		public function editarCliente(){
			$alm = new persona();

			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdCliente($_REQUEST['id']);
			}
			
			include_once 'view/save1.php';
		}

		// ACTUALIZAR REGISTRO DE CLIENTE
		public function guardar1(){
			$alm = new persona();
			
			//$alm->cedula_cliente = $this->mode->cargarCedula($_POST['cedula']);
			foreach ($this->mode->cargarIdCliente1($_POST['txtID']) as $l) : 
			$alm->id = $_POST['txtID'];
			$alm->cedulax = $_POST['cedula'];
			$alm->nombre = $_POST['nombres'];
			//$alm->apellido = $_POST['apellidos'];
			$alm->direccion = $_POST['direccion'];
			$alm->correo = $_POST['correo'];
			$alm->telefono = $_POST['telefono'];
			$alm->status = $_POST['status'];

			if($l->cedula == $alm->cedulax = $_POST['cedula']){
				$this->mode->actualizarCliente($alm);
				//header("Location: index.php");
				echo "<script>
                alert('Registro de Cliente Actualizado');
                 setTimeout( function() { window.location.href = 'index.php'; }, 1000 );
    			</script>";
			}else if($this->mode->cargarCedula2($_POST['cedula']) != ""){

				$this->mode->mensajeAmigoRegistrado1();
			}else {
				$this->mode->actualizarCliente($alm);
				//header("Location: index.php");
				echo "<script>
                alert('Registro de Cliente Actualizado2');
                 setTimeout( function() { window.location.href = 'index.php'; }, 1000 );
    			</script>";
			}
			endforeach; 


		
		}


		//ELIMINAR REGISTRO DEL CLIENTE SIEMPRE Y CUANDO NO POSEA DEUDA

		public function eliminarCliente(){
			$alm = new persona();
			
		
			foreach ($this->mode->consultarBalance($_REQUEST['id']) as $k) : 


			if($k->balance == null || $k->balance == "" || $k->balance ==0){
				$this->mode->deleteCliente($_REQUEST['id']);
				$this->mode->clienteEliminado();
	
			}else if($k->balance <0){
				$this->mode->clienteEnPositivo();
			}else{
				$this->mode->mensajeNoPuedeEliminar();
			}
			
			include_once 'view/clientes.php';

		endforeach; 
		}


		//VISTA PARA ASIGAR DEUDA
		public function asignarDeuda(){
			$alm = new persona();

			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdDeuda($_REQUEST['id']);
			}
			
			include_once 'view/saveDeuda.php';
		}

		//GUARDAR DEUDA 
		public function guardarDeuda(){
			$alm = new persona();
			$alm->idDeuda = $_POST['txtIdDeuda'];
			$alm->fecha = $_POST['fecha'];
			$alm->descripcion = $_POST['descripcion'];
			$alm->monto = $_POST['monto'];
			$alm->selectCliente1 = $_POST['selectCliente1'];
			$alm->precioDolarA = $_POST['precioDolarA'];
			$alm->montoEnDolares = floatval($alm->monto)/floatval($alm->precioDolarA);
			//$alm->selectCliente = $_POST['selectCliente'];
			
		
			if($alm->idDeuda > 0){
			$this->mode->actualizarDatosDeuda($alm);

			foreach ($this->mode->balancePositivoR($_POST['selectCliente1']) as $k):
			$alm->balancePositivo = $k->montox;
			endforeach;

			foreach ($this->mode->balanceNegativoR($_POST['selectCliente1']) as $k):
			$alm->balanceNegativo = $k->deuda;
			endforeach;

			$alm->balance=floatval($alm->balanceNegativo)-floatval($alm->balancePositivo);
		
			$this->mode->actualizarBalance1($alm);
			
			echo "<script>
				//console.log('<?php ?>');
                alert('Deuda actualizada');
                 setTimeout( function() { window.location.href = 'index.php'; }, 5000 );
    			</script>";

			}
			else{
				
			$alm->selectCliente = $_POST['selectCliente'];
			
			$this->mode->registrarDeuda($alm);

			foreach ($this->mode->balancePositivoR($_POST['selectCliente']) as $k):
			$alm->balancePositivo = $k->montox;
			endforeach;

			foreach ($this->mode->balanceNegativoR($_POST['selectCliente']) as $k):
			$alm->balanceNegativo = $k->deuda;
			endforeach;

			$alm->balance=floatval($alm->balanceNegativo)-floatval($alm->balancePositivo);
		
			$this->mode->actualizarBalance($alm);
			echo "<script>
				//console.log('<?php ?>');
                alert('Deuda Registrada');
                 setTimeout( function() { window.location.href = 'index.php'; }, 5000 );
    			</script>";
			}
			
			

		}

		//VISTA PARA EDITAR DEUDA
		public function editarDeuda(){
			$alm = new persona();

			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdDeuda($_REQUEST['id']);
			}
			
			include_once 'view/saveDeuda.php';
		}


		//PROCESO PARA REASIGNAR UNA DEUDA 
		public function reasignarDeuda(){
			$alm = new persona();

			if(isset($_REQUEST['id'])){
				$alm1 = $this->mode->cargarIdDeuda($_REQUEST['id']);
			}
			
			$this->mode->deleteDeuda($_REQUEST['id']);

			foreach ($this->mode->balancePositivoR($_REQUEST['idC']) as $k):
			$alm->balancePositivo = $k->montox;
			endforeach;

			foreach ($this->mode->balanceNegativoR($_REQUEST['idC']) as $k):
			$alm->balanceNegativo = $k->deuda;
			endforeach;

			$alm->selectCliente =($_REQUEST['idC']);
			
			$alm->balance=floatval($alm->balanceNegativo)-floatval($alm->balancePositivo);

			$this->mode->actualizarBalance($alm);

			
			include_once 'view/saveDeuda1.php';
		}

		//REASIGNAR LA DEUDA LUEGO DE ELIMINAR SE REGISTRA A OTRO CLIENTE
		public function guardarDeuda1(){
			$alm = new persona();


			$alm->idDeuda = $_POST['txtIdDeuda'];
			$alm->fecha = $_POST['fecha'];
			$alm->descripcion = $_POST['descripcion'];
			$alm->monto = $_POST['monto1'];
			$alm->selectCliente1 = $_POST['selectCliente1'];
			$alm->precioDolarA = $_POST['precioDolarA'];
			$alm->montoEnDolares = floatval($alm->monto)/floatval($alm->precioDolarA);
			$alm->selectCliente = $_POST['selectCliente'];
		
		
			$this->mode->registrarDeuda($alm);
			//actualizar balance de quien tiene la deuda
			foreach ($this->mode->balancePositivoR($_POST['selectCliente1']) as $k):
			$alm->balancePositivo = $k->montox;
			endforeach;

			foreach ($this->mode->balanceNegativoR($_POST['selectCliente1']) as $k):
			$alm->balanceNegativo = $k->deuda;
			endforeach;

			$alm->balance=floatval($alm->balanceNegativo)-floatval($alm->balancePositivo);
		
			$this->mode->actualizarBalance1($alm);
			
			//actualizar balance a quien se le asigna la deuda
			foreach ($this->mode->balancePositivoR($_POST['selectCliente']) as $k):
			$alm->balancePositivo = $k->montox;
			endforeach;

			foreach ($this->mode->balanceNegativoR($_POST['selectCliente']) as $k):
			$alm->balanceNegativo = $k->deuda;
			endforeach;

			$alm->balance=floatval($alm->balanceNegativo)-floatval($alm->balancePositivo);
		
			$this->mode->actualizarBalance($alm);

			echo "<script>
				//console.log('<?php ?>');
                alert('Deuda Registrada');
                 setTimeout( function() { window.location.href = 'index.php'; }, 5000 );
    			</script>";
			
			
			//header("Location: index.php");
		}

		//ELIMINAR DEUDA A CLIENTE
		public function eliminarDeuda(){
			$alm = new persona();

			$this->mode->deleteDeuda($_REQUEST['id']);

			//$alm->selectCliente = $_POST['selectCliente'];

			foreach ($this->mode->balancePositivoR($_REQUEST['idC']) as $k):
			$alm->balancePositivo = $k->montox;
			endforeach;

			foreach ($this->mode->balanceNegativoR($_REQUEST['idC']) as $k):
			$alm->balanceNegativo = $k->deuda;
			endforeach;

			$alm->selectCliente =($_REQUEST['idC']);
			$alm->balance=floatval($alm->balanceNegativo)-floatval($alm->balancePositivo);

			$this->mode->actualizarBalance($alm);
			
			
			include_once 'view/ultimasDeudas.php';
		}



		//VISTA PARA ASIGAR ABONO
		public function asignarPago(){
			$alm = new persona();

			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdPago($_REQUEST['id']);
			}
			
			include_once 'view/savePago.php';
		}

		//GUARDAR ABONO DEL CLIENTE
		public function guardarAbono(){
			$alm = new persona();
			$alm->idAbono = $_POST['txtIdAbono'];
			$alm->fecha = $_POST['fecha'];
			$alm->descripcion = $_POST['descripcion'];
			$alm->monto = $_POST['monto'];
			//$alm->selectCliente = $_POST['selectCliente'];
			$alm->selectCliente1 = $_POST['selectCliente1'];
			$alm->precioDolarA = $_POST['precioDolarA'];
			$alm->montoEnDolares = floatval($alm->monto)/floatval($alm->precioDolarA);



			if($alm->idAbono > 0){
			$this->mode->actualizarDatosAbono($alm);

			foreach ($this->mode->balancePositivoR($_POST['selectCliente1']) as $k):
			$alm->balancePositivo = $k->montox;
			endforeach;

			foreach ($this->mode->balanceNegativoR($_POST['selectCliente1']) as $k):
			$alm->balanceNegativo = $k->deuda;
			endforeach;

			$alm->balance=floatval($alm->balanceNegativo)-floatval($alm->balancePositivo);
		
			$this->mode->actualizarBalance1($alm);
			
			echo "<script>
				//console.log('<?php ?>');
                alert('Abono actualizado');
                 setTimeout( function() { window.location.href = 'index.php'; }, 5000 );
    			</script>";

			}
			else{
			$alm->selectCliente = $_POST['selectCliente1'];
			//$alm->selectCliente = $_POST['selectCliente'];
				$this->mode->registrarAbono($alm);

			foreach ($this->mode->balancePositivoR($_POST['selectCliente1']) as $k):
			$alm->balancePositivo = $k->montox;
			endforeach;

			foreach ($this->mode->balanceNegativoR($_POST['selectCliente1']) as $k):
			$alm->balanceNegativo = $k->deuda;
			endforeach;

			$alm->balance=floatval($alm->balanceNegativo)-floatval($alm->balancePositivo);
		
			$this->mode->actualizarBalance($alm);
			echo "<script>
				//console.log('<?php ?>');
                alert('Abono Registrada');
                 setTimeout( function() { window.location.href = 'index.php'; }, 5000 );
    			</script>";
			}
		}
			//EDITAR EL ABONO DEL CLIENTE
		public function editarAbono(){
			$alm = new persona();

			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdPago($_REQUEST['id']);
			}
			
			include_once 'view/savePago.php';
		}


		//REASIGNAR ABONO DEL CLIENTE
		public function reasignarAbono(){
			$alm = new persona();

			if(isset($_REQUEST['id'])){
				$alm1 = $this->mode->cargarIdPago($_REQUEST['id']);
			}
			
			$this->mode->deleteAbono($_REQUEST['id']);

			foreach ($this->mode->balancePositivoR($_REQUEST['idC']) as $k):
			$alm->balancePositivo = $k->montox;
			endforeach;

			foreach ($this->mode->balanceNegativoR($_REQUEST['idC']) as $k):
			$alm->balanceNegativo = $k->deuda;
			endforeach;

			$alm->selectCliente =($_REQUEST['idC']);
			
			$alm->balance=floatval($alm->balanceNegativo)-floatval($alm->balancePositivo);

			$this->mode->actualizarBalance($alm);

			
			include_once 'view/savePago1.php';
		}

		//ELIMINAR ABONO DEL CLIENTE
		public function eliminarAbono(){
			$alm = new persona();

			$this->mode->deleteAbono($_REQUEST['id']);

			//$alm->selectCliente = $_POST['selectCliente'];

			foreach ($this->mode->balancePositivoR($_REQUEST['idC']) as $k):
			$alm->balancePositivo = $k->montox;
			endforeach;

			foreach ($this->mode->balanceNegativoR($_REQUEST['idC']) as $k):
			$alm->balanceNegativo = $k->deuda;
			endforeach;

			$alm->selectCliente =($_REQUEST['idC']);
			$alm->balance=floatval($alm->balanceNegativo)-floatval($alm->balancePositivo);

			$this->mode->actualizarBalance($alm);
			
			
			include_once 'view/ultimosAbonos.php';
		}


		//REGISTRAR UN ABONO LUEGO DE SER REASIGNADO
		public function guardarAbono1(){
			$alm = new persona();


			$alm->idAbono = $_POST['txtIdAbono'];
			$alm->fecha = $_POST['fecha'];
			$alm->descripcion = $_POST['descripcion'];
			$alm->monto = $_POST['monto1'];
			$alm->selectCliente = $_POST['selectCliente'];
			$alm->selectCliente1 = $_POST['selectCliente1'];
			$alm->precioDolarA = $_POST['precioDolarA'];
			$alm->montoEnDolares = floatval($alm->monto)/floatval($alm->precioDolarA);
			$alm->selectCliente = $_POST['selectCliente'];
		
		
			$this->mode->registrarAbono($alm);

			//actualizar el balance de la persona a quien se le quita el abono
			foreach ($this->mode->balancePositivoR($_POST['selectCliente1']) as $k):
			$alm->balancePositivo = $k->montox;
			endforeach;

			foreach ($this->mode->balanceNegativoR($_POST['selectCliente1']) as $k):
			$alm->balanceNegativo = $k->deuda;
			endforeach;

			$alm->balance=floatval($alm->balanceNegativo)-floatval($alm->balancePositivo);
		
			$this->mode->actualizarBalance1($alm);

			// actualizar balance de quien se le esta colocando el abono
			foreach ($this->mode->balancePositivoR($_POST['selectCliente']) as $k):
			$alm->balancePositivo = $k->montox;
			endforeach;

			foreach ($this->mode->balanceNegativoR($_POST['selectCliente']) as $k):
			$alm->balanceNegativo = $k->deuda;
			endforeach;

			$alm->balance=floatval($alm->balanceNegativo)-floatval($alm->balancePositivo);
		
			$this->mode->actualizarBalance($alm);
			echo "<script>
				//console.log('<?php ?>');
                alert('Abono Registrado');
                 setTimeout( function() { window.location.href = 'index.php'; }, 5000 );
    			</script>";
			
			
			//header("Location: index.php");
		}


		//FUNCIONAR PARA REGISTRAR UN ABONO DEL CLIENTE EN BD CARGA VISTA SAVEPAGO2
		public function abonarCliente(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdCliente($_REQUEST['id']);
				//$alm1 = $this->mode->cargarIdPago($_REQUEST['id']);
			}
			include_once 'view/savePago2.php';
		}


		//LISTAR REGISTROS DE LA BD

		public function listarClientesDeudores(){
			$alm = new persona();

			/*if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdDeudores($_REQUEST['id']);
			}*/
			//$alm->precioDolarA = $_POST['precioDolarA'];
			//$alm = $this->mode->actualizarPrecioDolar();	
			$alm =$this->mode->consultarMontoTotalPorCobrar();		
			include_once 'view/listarClientesDeudores.php';
		}
		public function listarClientesConMas15DiasSinPagar(){
			$alm = new persona();

			/*if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdDeudores($_REQUEST['id']);
			}*/
			//$alm->precioDolarA = $_POST['precioDolarA'];
			//$alm = $this->mode->actualizarPrecioDolar();	
			$alm =$this->mode->consultarMontoTotalPorCobrar();		
			include_once 'view/listarClientesDeudoresMas15Dias.php';
		}
		public function listarClientesDeudoresMas100(){
			$alm = new persona();

			/*if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdDeudores($_REQUEST['id']);
			}*/
			//$alm->precioDolarA = $_POST['precioDolarA'];
			//$alm = $this->mode->actualizarPrecioDolar();	
			$alm =$this->mode->consultarMontoTotalPorCobrar();		
			include_once 'view/listarClientesDeudoresMas100.php';
		}
		public function listarUltimosPagos(){
			$alm = new persona();

			/*if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdDeudores($_REQUEST['id']);
			}*/
			//$alm->precioDolarA = $_POST['precioDolarA'];
			//$alm = $this->mode->actualizarPrecioDolar();	
			//$alm =$this->mode->consultarMontoTotalPorCobrar();		
			include_once 'view/ultimosAbonos.php';
		}

		public function listarUltimasDeudas(){
			$alm = new persona();

			/*if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdDeudores($_REQUEST['id']);
			}*/
			//$alm->precioDolarA = $_POST['precioDolarA'];
			//$alm = $this->mode->actualizarPrecioDolar();	
			//$alm =$this->mode->consultarMontoTotalPorCobrar();		
			include_once 'view/ultimasDeudas.php';
		}




		//ACTUALIZAR EL PRECIO DEL DOLAR Y ALMACENAR EN BD
		public function actualizarPrecioDolar(){
			$alm = new persona();
			$alm->precioDolarA = $_POST['precioDolarA'];
			$this->mode->actualizarPrecioDolar($alm);

			
		
			echo "<script>
				console.log('<?php ?>');
                alert('Precio Del Dolar Actualizado');
                 setTimeout( function() { window.location.href = 'index.php?c=listarClientesDeudores'; }, 1000 );
    			</script>";
		}

































































































































/*public function cambiarBalance(){
			$alm = new persona();

			foreach ($this->mode->balancePositivoR($_REQUEST['idC']) as $k):
			$alm->balancePositivo = $k->montox;
			endforeach;

			foreach ($this->mode->balanceNegativoR($_REQUEST['idC']) as $k):
			$alm->balanceNegativo = $k->deuda;
			endforeach;

			$alm->selectCliente =($_REQUEST['idC']);
			$alm->balance=floatval($alm->balanceNegativo)-floatval($alm->balancePositivo);

			$this->mode->actualizarBalance($alm);

			include_once 'view/ultimasDeudas.php';
			
		}*/




		public function actualizarMiembro(){
			$alm = new persona();
			
			$alm->id = $_POST['txtIdAmigo'];
			$alm->cedula = $_POST['cedula'];
			$alm->nombre = $_POST['nombres'];
			$alm->apellido = $_POST['apellidos'];
			$alm->sexo = $_POST['selectGenero'];
			$alm->direccion = $_POST['direccion'];
			$alm->codTlfn = $_POST['codTlfn'];
			$alm->telefono = $_POST['telefono'];
			$alm->fecha_nacimiento = $_POST['fechaNac'];
			$alm->fecha_ingreso = $_POST['fechaIng'];
			$alm->llegoPor = $_POST['selectLlegoPor'];
			
			$alm->id_miembro = $_POST['txtID'];
			$alm->fecha_paso_de_fe = $_POST['fechaMig'];
			$alm->fecha_bautismo = $_POST['fechaBau'];
			$alm->membresia = $_POST['membresia'];
			$alm->id_cargo = $_POST['selectCargo'];
			
			$alm->vehiculo = $_POST['selectVehiculo'];
			$alm->nivelAcademico = $_POST['selectNivelEducativo'];
			$alm->profesion = $_POST['profesion'];

			$this->mode->actualizarDatosMiembro($alm);
			$this->mode->actualizarDatosAmigo($alm);
			echo "<script>
                alert('Data actualizada');
                 setTimeout( function() { window.location.href = 'index.php?c=listarMiembros'; }, 1000 );
    			</script>";
			//header("Location: index.php");
		}
		

		public function inhabilitarDonacion(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->inhabilitarDonacion($_REQUEST['id']);
			}
			//$alm=$this->mode->inhabilitarActividad();
			//$alm1 = $this->mode->cargarCantActividades();
			include_once 'view/homeDonaciones.php';
		}
		public function habilitarDonacion(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->habilitarDonacion($_REQUEST['id']);
			}
			//$alm=$this->mode->inhabilitarActividad();
			//$alm1 = $this->mode->cargarCantActividadesInactivas();
			include_once 'view/homeDonacionesInactivas.php';
		}


		public function inhabilitarActividad(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->inhabilitarActividad($_REQUEST['id']);
			}
			//$alm=$this->mode->inhabilitarActividad();
			$alm1 = $this->mode->cargarCantActividades();
			include_once 'view/homeActividades.php';
		}
		public function habilitarActividad(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->habilitarActividad($_REQUEST['id']);
			}
			//$alm=$this->mode->inhabilitarActividad();
			$alm1 = $this->mode->cargarCantActividadesInactivas();
			include_once 'view/homeActividadesInactivas.php';
		}
		public function asignarDonacion(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdDonacion($_REQUEST['id']);
			}
			include_once 'view/asignarDonacion.php';
		}
		public function asignarDonacionActividad(){
			$alm = new persona();
			/*if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdDonacion($_REQUEST['id']);
			}*/
			
			$alm->descripcion= $_POST['descripcion'];
			$alm->cantidad = $_POST['cantidad'];
			$alm->fecha = $_POST['fecha'];
			//$alm->donacion_id = $_POST['apellidos'];
			$alm->id = $_POST['txtIdDonacion'];
			$alm->actividad_id = $_POST['selectActividad'];
			$this->mode->asignarDonacionActividad($alm);
			//$alm = $this->mode->asignarDonacionActividad($alm);
			include_once 'view/homeDonaciones.php';
		}
		
		public function nuevoActividad(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdActividad($_REQUEST['id']);
			}
			include_once 'view/saveActividad.php';
		}
		public function nuevaDonacion(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdDonacion($_REQUEST['id']);
			}
			include_once 'view/saveDonacion.php';
		}
		/*public function asignarDonacion(){
			$alm = new persona();
			//$alm1 = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdDonacion($_REQUEST['id']);
				//$alm1 = $this->mode->cargarAmigosTablaGrupos();
			}
			include_once 'view/asignarDonacion.php';
		}*/

		public function asignarAmigo(){
			$alm = new persona();
			//$alm1 = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdGrupo($_REQUEST['id']);
				//$alm1 = $this->mode->cargarAmigosTablaGrupos();
			}
			include_once 'view/homeAsignarAmigo.php';
		}
		public function asignarAmigo1(){
			//$alm = new persona();
			$alm1 = new persona();
			if(isset($_REQUEST['id'])){
				//$alm = $this->mode->cargarIdGrupo($_REQUEST['id']);
				$alm1 = $this->mode->cargarAmigosTablaGrupos();
			}
			include_once 'view/homeAsignarAmigo.php';
		}

		public function nuevoGrupoFam(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdGrupo($_REQUEST['id']);
			}
			include_once 'view/saveGupo.php';
		}
		public function listarAmigos(){
			$alm = new persona();
			$alm = $this->mode->cantAmigos();
			include_once 'view/home.php';
		}
		public function listarGrupos(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdGrupo($_REQUEST['id']);
			}
			$alm = $this->mode->cargarCantGrupos();
			include_once 'view/homeGrupos.php';
		}
		public function listarDonaciones(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdDonacion($_REQUEST['id']);
			}
			//$alm1 = $this->mode->cargarCantGrupos();
			include_once 'view/homeDonaciones.php';
		}
		public function listarMiembros(){
			$alm = new persona();
			$alm = $this->mode->cantMiembros();
			include_once 'view/homeMiembros.php';
		}
		public function listarActividades(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdActividad($_REQUEST['id']);
			}
			$alm1 = $this->mode->cargarCantActividades();
			include_once 'view/homeActividades.php';
		}
		public function actividadesInactivas(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdActividadInactiva($_REQUEST['id']);
			}
			$alm1 = $this->mode->cargarCantActividadesInactivas();
			include_once 'view/homeActividadesInactivas.php';
		}
		public function donacionesInactivas(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				//$alm = $this->mode->cargarIdDonacionInactiva($_REQUEST['id']);
			}
			//$alm1 = $this->mode->cargarCantDonacionesInactivas();
			include_once 'view/homeDonacionesInactivas.php';
		}
		public function donacionesAsignadas(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				//$alm = $this->mode->cargarIdDonacionInactiva($_REQUEST['id']);
			}
			//$alm1 = $this->mode->cargarCantDonacionesInactivas();
			include_once 'view/homeDonacionesAsignadas.php';
		}
		public function amigosGrupoFam(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarAmigosGrupo($_REQUEST['id']);
			}
			include_once 'view/homeGrupoAmigos.php';
		}
		/*public function guardar(){
			$alm = new persona();
			$alm->id = $_POST['txtID'];
			$alm->cedula = $_POST['cedula'];
			$alm->nombre = $_POST['nombres'];
			$alm->apellido = $_POST['apellidos'];
			$alm->sexo = $_POST['selectGenero'];
			$alm->direccion = $_POST['direccion'];
			$alm->telefono = $_POST['telefono'];
			$alm->fecha_nacimiento = $_POST['fechaNac'];
			
			
			$alm->id > 0 ? $this->mode->actualizarDatos($alm) : $this->mode->registrar($alm);
			header("Location: index.php");
		}*/
		
		public function guardarActividad(){
			$alm = new persona();
			$alm->id = $_POST['txtIdActividad'];
			$alm->nombre = $_POST['nombreActividad'];
			$alm->direccion = $_POST['direccionActividad'];
			$alm->fecha = $_POST['fechaActividad'];
			$alm->hora = $_POST['horaActividad'];
			$alm->descripcion = $_POST['descripcionActividad'];
			$alm->lider_id = $_POST['selectLider'];
			
			
			
			$alm->id > 0 ? $this->mode->actualizarDatosActividad($alm) : $this->mode->registrarActividad($alm);
			header("Location: index.php");
		}
		public function guardarDonacion(){
			$alm = new persona();
			$alm->id = $_POST['txtIdDonacion'];
			$alm->detalles = $_POST['detallesDonacion'];
			$alm->cantidad = $_POST['cantidad'];
			$alm->donante_id = $_POST['selectDonante'];
			$alm->fecha = $_POST['fechaDonacion'];
			
			
			$alm->id > 0 ? $this->mode->actualizarDatosDonacion($alm) : $this->mode->registrarDonacion($alm);
			header("Location: index.php");
		}
		
		public function guardarAmigoGrupo(){
			$alm = new persona();
			$alm->id_grupo = $_POST['txtIdGrupo'];
			$alm->id_amigo = $_POST['selectAmigo'];
			$alm->id_amigoTabla = $_POST['amigoTabla'];
			if($alm->id_amigo == $alm->id_amigoTabla ){

			}else{
				$alm->id_grupo > 0 ? $this->mode->registrarAmigoEnGrupo($alm) : $this->mode->registrarAmigoEnGrupo($alm);
			}
			//$alm->id_grupo > 0 ? $this->mode->registrarAmigoEnGrupo($alm) : $this->mode->registrarAmigoEnGrupo($alm);
			header("Location: index.php?c=listarGrupos");
		}
		/*public function eliminar(){
			$this->mode->delete($_REQUEST['id']);
			//header("Location: index.php");
			include_once 'view/home.php';
		}*/
		public function editarMiembros(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdMiembro($_REQUEST['id']);
				
			}
			//header("Location: index.php");
			include_once 'view/saveMiembros.php';
		}
		public function actualizarDonacionAsignada(){
			$alm = new persona();
			
			$alm->id = $_POST['txtIdDonacionAsignada'];
			$alm->descripcion = $_POST['descripcion'];
			$alm->cantidad = $_POST['cantidad'];
			$alm->fecha = $_POST['fecha'];
			$alm->actividad_id = $_POST['selectActividad'];

			$this->mode->actualizarDatosDonacionAsignada($alm);
			echo "<script>
                alert('Data actualizada');
                 setTimeout( function() { window.location.href = 'index.php?c=donacionesAsignadas'; }, 1000 );
    			</script>";
			//header("Location: index.php");
		}
		public function editarDonacionAsignada(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarIdDetallesDonacion($_REQUEST['id']);
				
			}
			//header("Location: index.php");
			include_once 'view/editarDetallesDonacion.php';
		}
		public function eliminar(){
			$alm = new persona();
			$this->mode->delete($_REQUEST['id']);
			
    			/*echo "<script>
                alert('Miembro Registrado');
                 setTimeout( function() { window.location.href = 'index.php'; }, 1000 );
    			</script>";*/
			$alm = $this->mode->cantAmigos();
			include_once 'view/home.php';
		}
		public function eliminarMiembros(){
			$alm = new persona();
			$this->mode->deleteMiembro($_REQUEST['id']);
			$this->mode->cambiarStatus($_REQUEST['amigo_id']);
			//$this->mode->cambiarStatus($_REQUEST['idAmigo']);
			$alm = $this->mode->cantMiembros();
			//header("Location: index.php");
			include_once 'view/homeMiembros.php';
		}
	
		public function eliminarGrupo(){
			$this->mode->deleteGrupo($_REQUEST['id']);
			//header("Location: index.php");
			//include_once 'index.php?c=listarGrupos';
			header("Location: index.php?c=listarGrupos");
		}
		public function eliminarAmigoDeGrupo(){
			$this->mode->deleteAmigoDeGrupo($_REQUEST['id']);
			//header("Location: index.php");
			//include_once 'view/homeGrupos.php';
			header("Location: index.php?c=listarGrupos");
		}
		public function eliminarActividad(){
			$this->mode->deleteActividad($_REQUEST['id']);
			header("Location: index.php?c=listarActividades");
			//include_once 'view/homeGrupos.php';
		}
		public function eliminarDonacion(){
			$this->mode->deleteDonacion($_REQUEST['id']);
			header("Location: index.php?c=listarDonaciones");
			//include_once 'view/homeGrupos.php';
		}
		public function eliminarDonacionAsignada(){
			$this->mode->deleteDonacionAsignada($_REQUEST['id']);
			header("Location: index.php?c=listarDonaciones");
			//include_once 'view/homeGrupos.php';
		}
	}
 ?>