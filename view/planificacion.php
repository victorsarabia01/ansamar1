<!DOCTYPE html>
<html>
<head>
    
    <link rel="stylesheet" href="resources/css/bootstrap.css">
    <script type="text/javascript" src="resources/js/bootstrap.min.js"> </script>

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>


    <script type="text/javascript" src="resources/js/Jquery.js"></script>
    
</head>
<body>
    <br>
<div class="row justify-content-center">
<div class="card text-dark bg-light mb-3" style="max-width: 40rem;">
  
  <div class="card-body">
                        <div class="alert alert-success" role="alert">
                            <h3>Planificacion de atencion en consultorios</h3>
                        </div>
                       
                    <form class="form-horizontal" method="post" action="?c=guardarPlanificacion">
              

                        <div class="col-md-8">
                           
                             <select name="consultorio" id="consultorio" class="form-select form-select-lg mb-1" aria-label="Ejemplo de .form-select-lg" required>
                                <option value="0">Consultorio</option>
                                <?php foreach ($this->mode->listarTodosConsultorios()  as $k) : ?>
                                    <option value="<?php echo $k->id ?>"> <?php echo $k->descripcion ?></option>
                                <?php endforeach ?>
                            </select>
                            <!--<select name="turno" id="turno" class="form-select form-select-lg mb-1" aria-label="Ejemplo de .form-select-lg" required>
                               
                                <?php foreach ($this->mode->listarTodosTurnos()  as $k) : ?>
                                    <option value="<?php echo $k->id ?>"> <?php echo $k->descripcion ?></option>
                                <?php endforeach ?>
                            </select>-->
                            <select name="turno" id="turno" class="form-select form-select-lg mb-1" aria-label="Ejemplo de .form-select-lg" required>
                                <option value="0">Turno</option>
                                <option value="1">Mañana</option>
                                <option value="2">Tarde</option>
                            </select>
                    
                            <select name="doctor" id="doctor" class="form-select form-select-lg mb-1" aria-label="Ejemplo de .form-select-lg" required>
                              <option value="0">Odontologo</option>
                                <?php foreach ($this->mode->listarTodosDoctores()  as $k) : ?>
                                    <option value="<?php echo $k->id ?>"> <?php echo $k->nombre." ".$k->apellido?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                  

                        <br>

                  
                            <button type="submit" href="?c=guardar" value="Guardar" name="registrar" id="registrar" class="btn btn-success">Guardar Planificacion</button>
                        
                            <a href="index.php" class="btn btn-block btn-danger">Cancelar</a>
                     
                        
            </form>
  </div>
</div>
</div>






<script>
    
    // Forzar solo números y puntos decimales
    function keepNumOrDecimal(obj) {
     // Reemplace todos los no numéricos primero, excepto la suma numérica.
    obj.value = obj.value.replace(/[^\d.]/g,"");
     // Debe asegurarse de que el primero sea un número y no.
    obj.value = obj.value.replace(/^\./g,"");
     // Garantizar que solo hay uno. No más.
    obj.value = obj.value.replace(/\.{2,}/g,".");
     // Garantía. Solo aparece una vez, no más de dos veces
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
    }
    </script>

    <script type="text/javascript"> // VALIDAR CAMPOS DE SOLO NUMERO Y LETRAS AL INPUT
                          //jQuery('.soloNumeros').keypress(function (tecla) {
                          //if (tecla.charCode < 48 || tecla.charCode > 57) return false;
                          //});
                          
                          $("input.buscar").bind('keypress', function(event) {
                          var regex = new RegExp("^[a-zA-Z ]+$");
                          var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                          if (!regex.test(key)) {
                          event.preventDefault();
                          return false;
                          }
                          });
    </script>


    
</body>
</html>