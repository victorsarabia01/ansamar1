<?php
	//include_once 'controller/control.php';
?>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="resources/css/bootstrap.css">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h3>Donaciones Asignadas</h3>
			</div>
		</div>
		<div class="">
		
			<a href="index.php?c=listarDonaciones" class="btn btn-block btn-danger">Salir</a>
		</div>
		
		<br>
		<div class="row">
			<div class="col-md-12 text-center">
				<table class="table">
					<tr class="table-secondary">
					
						<th>Donacion</th>
						<th>Descripcion</th>
						<th>Cantidad Donada</th>
						<th>Fecha Donacion</th>
						<th>Actividad</th>
						<th></th>
						<th></th>
						<th></th>
					
						
						
					</tr>
					<?php foreach ($this->mode->listarDonacionesAsignadas() as $k) : ?>
						
						<tr>
							<td><?php echo $k->nombreDonacion; ?></td>
							<td><?php echo $k->descripcion; ?></td>
							<td><?php echo $k->cantidad; ?></td>
							<td><?php echo $newDate = date("d-m-Y", strtotime($k->fechaEntregada)); ?></td>
							<td><?php echo $k->nombre; ?></td>
						

							<td>
								<a href="?c=editarDonacionAsignada&id=<?php echo $k->id; ?>" class="btn btn-warning">Editar</a>
							</td>
							<td>
								<a href="?c=eliminarDonacionAsignada&id=<?php echo $k->id; ?>" class="btn btn-danger">Eliminar</a>
							</td>

						</tr>

				<?php endforeach; ?>
					
				</table>
				<div class="row">
				<a href="index.php?c=listarDonaciones" class="btn btn-block btn-danger">Salir</a>
				</div>
				
			</div>
		</div>
	</div>

</body>
</html>