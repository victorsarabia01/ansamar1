 <fieldset>
                        <legend class="header">Registro de Personas</legend>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                            <div class="col-md-8">
                            	<input type="hidden" name="txtID" value="<?php echo $alm->id; ?>">
                                <input  name="txtNombre" type="text" value="<?php echo $alm->nombre; ?>" placeholder="nombre" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                            <div class="col-md-8">
                                <input  name="txtApellido" type="text" value="<?php echo $alm->apellido; ?>" placeholder="Apellido" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope-o bigicon"></i></span>
                            <div class="col-md-8">
                                <input  name="txtEdad" type="text" value="<?php echo $alm->edad; ?>" placeholder="Edad" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon"></i></span>
                            <div class="col-md-8">
                                <input  name="fecha" type="date" value="<?php echo $alm->fecha; ?>" placeholder="fecha" class="form-control">
                            </div>
                        </div>

				        <div class="form-group">
							<div class="col-md-3">
								<select name="genero">
									<?php foreach ($this->mode->cargarGenero()  as $k) : ?>
										<option value="<?php echo $k->id_genero ?>" <?php echo $k->id_genero == $alm->id_genero? 'selected' : ''; ?>><?php echo $k->nombre_genero ?></option>
									<?php endforeach ?>
								</select>
							</div>
						</div>
                        <div class="form-group">
                            <div class="">
                                
                               
                                
                            </div>
                        </div>
                    </fieldset>