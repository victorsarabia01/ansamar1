<!DOCTYPE html>
<html>
<head>
	<!--<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.css">
	<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>-->
	<link rel="stylesheet" href="resources/css/bootstrap.css">
	<script type="text/javascript" src="resources/js/bootstrap.min.js"> </script>
</head>
<body>
    <br>
<div class="row justify-content-center">
<div class="card text-dark bg-light mb-3" style="max-width: 40rem;">
  <div class="card-header">Editar Asignar Donacion</div>
  <div class="card-body">
					<form class="form-horizontal" method="post" action="?c=actualizarDonacionAsignada">
                        
                        <div class="col-md-8">
                            <input type="hidden" name="txtIdDonacionAsignada" value="<?php echo $alm->id; ?>">
                        </div>
                        <div class="col-md-8">
                            <!--<input type="hidden" name="amigoTabla" value="<?php echo $alm->amigo_id; ?>">-->
                            <label>Actividad: </label> 
                            
                            <select name="selectActividad">
                                <?php foreach ($this->mode->listarActividades()  as $k) : ?>
                                    <option value="<?php echo $k->id ?>" <?php echo $k->id == $alm->actividad_id ? 'selected' : ''; ?>><?php echo $k->nombre." ".$k->fecha." ".$k->descripcion ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        
                        <div class="col-md-8">
                            <input type="text" style="text-transform:uppercase;" class="form-control" name="descripcion" id="descripcion" value="<?php echo $alm->descripcion; ?>" aria-describedby="emailHelp" placeholder="descripcion" maxlength="80" required>
                        </div>
                         <div class="col-md-8">
                            <input type="text" style="text-transform:uppercase;" class="form-control" name="cantidad" id="cantidad" value="<?php echo $alm->cantidad; ?>" aria-describedby="emailHelp" placeholder="cantidad" maxlength="80" required>
                        </div>
                        <div class="col-md-8">
                            <label for="exampleInputEmail1" id="fechaA" class="form-label">Fecha Asignacion</label>
                            <input type="date" class="form-control" name="fecha" id="fecha" value="<?php echo $alm->fechaEntregada; ?>" aria-describedby="emailHelp" required>
                        </div>
                        <br>
                        <div>
                        <!--<a href="?c=guardar" class="btn btn-block btn-success">Guardar</a>-->
						<button type="submit" value="Guardar" class="btn btn-success">Guardar</button>
                        <a href="index.php?c=donacionesAsignadas" class="btn btn-block btn-danger">Cancelar</a>
                        </div>
                        <div>
                        
                        </div>
			</form>
  </div>
</div>
</div>









	
	<!--<script type="text/javascript" src="resources/js/Jquery.js"></script>
	<script type="text/javascript" src="resources/js/materialize.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('select').formSelect();
		});
	</script>-->
</body>
</html>