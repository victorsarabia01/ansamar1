<?php
	//include_once 'controller/control.php';
?>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="resources/css/bootstrap.css">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h3>Actualmente existen "<?php echo $alm1->cantActividades; ?>" Actividades Inactivas</h3>
			</div>
		</div>
		<div class="">
			<!--<a href="?c=nuevoActividad" class="btn btn-block btn-success">Nuevo Registro</a>
			<a href="#" class="btn btn-block btn-warning">Actividades Inactivas</a>-->
			<a href="index.php?c=listarActividades" class="btn btn-block btn-danger">Salir</a>
		</div>
		
		<br>
		<div class="row">
			<div class="col-md-12 text-center">
				<table class="table">
					<tr class="table-secondary">
					
						<th>Nombre</th>
						<th>Direccion</th>
						<th>Fecha</th>
						<th>Hora</th>
						<th>Descripcion</th>
						<th>Encargado</th>
						<th></th>
						<th></th>
						
						
						
					</tr>
					<?php foreach ($this->mode->listarActividadesInactivas() as $k) : ?>
						
						<tr>
							<td><?php echo $k->nombre; ?></td>
							<td><?php echo $k->direccion; ?></td>
							<td><?php echo $newDate = date("d-m-Y", strtotime($k->fecha)); ?></td>
							<td><?php echo $k->hora; ?></td>
							<td><?php echo $k->descripcion; ?></td>
							<td><?php echo $k->nombreEncargado; ?></td>

						
							<!--<td>
								<a href="?c=asignarAmigo&id=<?php echo $k->id;?>" class="btn btn-warning">AsignarAmigo</a>
							</td>
							<td>
								<a href="?c=amigosGrupoFam&id=<?php echo $k->id; ?>" class="btn btn-info">DetallesGrupo</a>
							</td>-->
							<td>
								<a href="?c=habilitarActividad&id=<?php echo $k->id; ?>" class="btn btn-warning">Habilitar</a>
							</td>
							<td>
								<a href="?c=eliminarActividad&id=<?php echo $k->id; ?>" class="btn btn-danger">Eliminar</a>
							</td>

						</tr>

				<?php endforeach; ?>
					
				</table>
				<div class="row">
				<a href="index.php?c=listarActividades" class="btn btn-block btn-danger">Salir</a>
				</div>
				
			</div>
		</div>
	</div>

</body>
</html>