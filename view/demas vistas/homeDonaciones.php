<?php
	//include_once 'controller/control.php';
?>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="resources/css/bootstrap.css">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h3>Donaciones</h3>
			</div>
		</div>
		<div class="">
			<a href="?c=nuevaDonacion" class="btn btn-block btn-success">Nuevo Registro</a>
			<a href="?c=donacionesInactivas" class="btn btn-block btn-warning">Donaciones Inactivas</a>
			<a href="?c=donacionesAsignadas" class="btn btn-block btn-info">Relacion de Donaciones</a>
			<a href="index.php" class="btn btn-block btn-danger">Salir</a>
		</div>
		
		<br>
		<div class="row">
			<div class="col-md-12 text-center">
				<table class="table">
					<tr class="table-secondary">
					
						<th>Detalles</th>
						<th>Cantidad</th>
						<th>Fecha Donacion</th>
						<th>Donante</th>
						
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						
						
					</tr>
					<?php foreach ($this->mode->listarDonaciones() as $k) : ?>
						
						<tr>
							<td><?php echo $k->detalles; ?></td>
							<td><?php echo $k->cantidad; ?></td>
							<td><?php echo $newDate = date("d-m-Y", strtotime($k->fecha)); ?></td>
							<td><?php echo $k->nombreDonante; ?></td>
						

							<td>
								<a href="?c=nuevaDonacion&id=<?php echo $k->id; ?>" class="btn btn-primary">>Editar<</a>
							</td>
							<!--<td>
								<a href="?c=asignarAmigo&id=<?php echo $k->id;?>" class="btn btn-warning">AsignarAmigo</a>
							</td>
							<td>
								<a href="?c=amigosGrupoFam&id=<?php echo $k->id; ?>" class="btn btn-info">DetallesGrupo</a>
							</td>-->
							<td>
								<a href="?c=asignarDonacion&id=<?php echo $k->id; ?>" class="btn btn-info">Asignar</a>
							</td>
							<td>
								<a href="?c=inhabilitarDonacion&id=<?php echo $k->id; ?>" class="btn btn-warning">Inhabilitar</a>
							</td>
							<td>
								<a href="?c=eliminarDonacion&id=<?php echo $k->id; ?>" class="btn btn-danger">Eliminar</a>
							</td>

						</tr>

				<?php endforeach; ?>
					
				</table>
				<div class="row">
				<a href="?c=nuevaDonacion" class="btn btn-block btn-success">Nuevo Registro</a>
				</div>
				
			</div>
		</div>
	</div>

</body>
</html>