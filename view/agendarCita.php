<!DOCTYPE html>
<html>
<head>
    
    <link rel="stylesheet" href="resources/css/bootstrap.css">
    <script type="text/javascript" src="resources/js/bootstrap.min.js"> </script>

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>


    <script type="text/javascript" src="resources/js/Jquery.js"></script>
    
</head>
<body>
    <br>
<div class="row justify-content-center">
<div class="card text-dark bg-light mb-3" style="max-width: 40rem;">
  
  <div class="card-body">
                        <div class="alert alert-success" role="alert">
                            <h3>Hola, Agenda tu cita Aqui</h3>
                            <h6>(solicitud de cita)</h6>
                        </div>
                       
                    <form class="form-horizontal" method="post" action="?c=guardar">
                        <div class="col-md-8">
                        <input type="hidden" name="txtID" id="txtID" value="<?php echo $alm->id; ?>">
                        <input type="hidden" name="cedulaAmigo" id="cedulaAmigo" value="<?php echo $alm->cedula; ?>">

                            <input type="text" class="form-control" name="cedula" id="cedula" onkeyup= keepNumOrDecimal(this) value="<?php echo $alm->cedula; ?>" aria-describedby="emailHelp" placeholder="Cedula Ejem. 22186490" maxlength="8" required>
                        </div>
              
                        <div class="col-md-8">
                            <input type="text" name="nombres" class="form-control mayusculas buscar" id="nombres" value="<?php echo $alm->nombresApellidos; ?>" aria-describedby="emailHelp" placeholder="Nombres" maxlength="25" required>
                        </div>
                         <div class="col-md-8">
                            <input type="text" name="apellidos" class="form-control mayusculas buscar" id="nombres" value="<?php echo $alm->nombresApellidos; ?>" aria-describedby="emailHelp" placeholder="Apellidos" maxlength="25" required>
                        </div>

                        <div class="col-md-8">
                        
                            <input type="text" class="form-control" name="telefono" onkeyup= keepNumOrDecimal(this) id="telefono" value="<?php echo $alm->tlfn; ?>" aria-describedby="emailHelp" placeholder="04245208619" maxlength="11" required>
                        </div>

                        <div class="col-md-8">
                            
                            <input type="date" class="form-control" id="fecha" name="fecha" required>
                        </div>
                        

                        <div class="col-md-8">

                                <!--<div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                    <label class="form-check-label" for="inlineRadio1">1</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                    <label class="form-check-label" for="inlineRadio2">2</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                    <label class="form-check-label" for="inlineRadio2">3</label>
                                </div>-->
                           
                           
                             <select name="consultorio" id="consultorio" class="form-select form-select-lg mb-1" aria-label="Ejemplo de .form-select-lg" required>
                                <option value="0">Consultorio</option>
                                <?php foreach ($this->mode->listarTodosConsultorios()  as $k) : ?>
                                    <option value="<?php echo $k->id ?>"> <?php echo $k->descripcion ?></option>
                                <?php endforeach ?>
                            </select>
                            <!--<select name="turno" id="turno" class="form-select form-select-lg mb-1" aria-label="Ejemplo de .form-select-lg" required>
                               
                                <?php foreach ($this->mode->listarTodosTurnos()  as $k) : ?>
                                    <option value="<?php echo $k->id ?>"> <?php echo $k->descripcion ?></option>
                                <?php endforeach ?>
                            </select>-->
                            <select name="turno" id="turno" class="form-select form-select-lg mb-1" aria-label="Ejemplo de .form-select-lg" required>
                                <option value="0">Turno</option>
                                <option value="1">Mañana</option>
                                <option value="2">Tarde</option>
                            </select>
                    
                            <select name="doctor" id="doctor" class="form-select form-select-lg mb-1" aria-label="Ejemplo de .form-select-lg" required>
                              <option value="0">Odontologo</option>
                                <?php foreach ($this->mode->listarTodosDoctores()  as $k) : ?>
                                    <option value="<?php echo $k->id ?>"> <?php echo $k->nombre." ".$k->apellido?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-md-8">
                            <div class="g-recaptcha" data-sitekey="6Lcc4xInAAAAAIhChEIZvj71HnTxRnwBqVgK6daJ"></div>
                        </div>


                        <br>

                  
                            <button type="submit" href="?c=guardar" value="Guardar" name="registrar" id="registrar" class="btn btn-success">Agendar</button>
                        
                            <a href="index.php" class="btn btn-block btn-danger">Cancelar</a>
                     
                        
            </form>
  </div>
</div>
</div>






<script>
    
    // Forzar solo números y puntos decimales
    function keepNumOrDecimal(obj) {
     // Reemplace todos los no numéricos primero, excepto la suma numérica.
    obj.value = obj.value.replace(/[^\d.]/g,"");
     // Debe asegurarse de que el primero sea un número y no.
    obj.value = obj.value.replace(/^\./g,"");
     // Garantizar que solo hay uno. No más.
    obj.value = obj.value.replace(/\.{2,}/g,".");
     // Garantía. Solo aparece una vez, no más de dos veces
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
    }
    </script>

    <script type="text/javascript"> // VALIDAR CAMPOS DE SOLO NUMERO Y LETRAS AL INPUT
                          //jQuery('.soloNumeros').keypress(function (tecla) {
                          //if (tecla.charCode < 48 || tecla.charCode > 57) return false;
                          //});
                          
                          $("input.buscar").bind('keypress', function(event) {
                          var regex = new RegExp("^[a-zA-Z ]+$");
                          var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                          if (!regex.test(key)) {
                          event.preventDefault();
                          return false;
                          }
                          });
    </script>


    <script language="JavaScript" type="text/javascript">
            $(document).ready(function(){
                
                 $('#doctor').click(function(){
                    console.log('hizo click');
                    var x = document.getElementById('consultorio').value;
                    var z = document.getElementById('turno').value;
                    console.log(x);
                    console.log(z);
                     //var odontologos = document.getElementById("doctor");
                        <?php foreach ($this->mode->filtrarDoctores()  as $k) : ?>
                            //console.log('x');
                            //alert( "<?php echo $k->nombre ?>");
                            
                            var aficiones = document.getElementById("doctor");
                            var inputAficion = '<?php echo $k->nombre ?>';
                            var option = document.createElement("option");
                            option.text = inputAficion;
                            option.value='<?php echo $k->idDoctor ?>';
                            aficiones.add(option);

                        <?php endforeach ?>

                });
            });
</script>

                    <script type="text/javascript">
                                $(document).ready(function() {
                                    $.ajax({
                                        type: "POST",
                                        url: "get_esp.php",
                                        success: function(response)
                                        {
                                            $('#especialidad').append(response);
                                        }
                                    });

                                });

                            $("#especialidad").change(function()
                                {

                                    var cod = $("#especialidad").val();

                                        $("#especialista option").remove();
                                        $("#fecha option").remove();
                                        $('#fecha').val('');

                                        $.ajax({
                                        type: "POST",
                                        url: "get_med.php?cod="+cod,
                                        success: function(response)
                                        {
                                         $('#especialista').append(response);
                                        }
                                        });

 
                                    });
                            </script>
            
            <script type="text/javascript">
                                $(document).ready(function() {
                                    $.ajax({
                                        type: "POST",
                                        url: "get_med.php",
                                        success: function(response)
                                        {
                                            $('#especialista').append(response);
                                        }
                                });

                                });

            </script>


    
</body>
</html>